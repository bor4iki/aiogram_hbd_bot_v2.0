import asyncio
import aioschedule
from aiogram import executor

from loader import dp, db, bot
import middlewares, filters, handlers
from utils.misc.rezerv_copy import RezervCopy
from utils.misc.test_new_notific import Notification
from utils.notify_admins import on_startup_notify
from utils.set_bot_commands import set_default_commands


async def scheduler():
    notifi = Notification(database=db, bot=bot)
    copy = RezervCopy('bot.db')

    aioschedule.every().day.at("10:00").do(notifi.run)
    aioschedule.every().day.at("10:00").do(copy.run)
    while True:
        await aioschedule.run_pending()
        await asyncio.sleep(1)


async def on_startup(dispatcher):
    # Устанавливаем дефолтные команды
    await set_default_commands(dispatcher)

    # Уведомляет про запуск
    await on_startup_notify(dispatcher)
    asyncio.create_task(scheduler())


if __name__ == '__main__':
    executor.start_polling(dp, on_startup=on_startup)

