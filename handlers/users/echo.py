from aiogram import types
from aiogram.dispatcher.filters import Text
from aiogram.dispatcher import FSMContext

from loader import dp, db
from utils.misc.add_new_user import add_user_to_db
from utils.misc.logs import logging


# Эхо хендлер, куда летят текстовые сообщения без указанного состояния
@dp.message_handler()
async def bot_echo(message: types.Message):
    logging.info(f'{message.from_user.full_name} write message {message.text}')
    await add_user_to_db(db, message)
    await message.answer(f"Отправь мне команду /new, для того, чтобы добавить важную дату, чтоб я напомнил тебе о ней!")
    # print(message)


# Эхо хендлер, куда летят ВСЕ сообщения с указанным состоянием


# @dp.message_handler(state="*", content_types=types.ContentTypes.ANY)
# async def bot_echo_all(message: types.Message, state: FSMContext):
#     state = await state.get_state()
#     await message.answer(f"Эхо в состоянии <code>{state}</code>.\n"
#                          f"\nСодержание сообщения:\n"
#                          f"<code>{message}</code>")
