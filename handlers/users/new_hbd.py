import asyncio
import re
from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Command, Text
from aiogram.types import ReplyKeyboardRemove

from keyboards.default.main_keyboard import main_menu
from loader import dp, db
from states.add_new_hbd import NewHbd
from keyboards.inline.yesorno import keyboard
from keyboards.inline.cancel_btn import cancel_btn
from utils.misc.add_new_user import add_user_to_db
from utils.misc.logs import logging


@dp.message_handler(Command('new'), state=None)
@dp.message_handler(Text(equals='Добавить новую запись'))
async def new_hbd(message: types.Message):
    logging.info(f'{message.from_user.full_name} start command /new')
    await add_user_to_db(db, message)
    await NewHbd.date_hbd.set()
    await message.answer(f'Итак, приступим к формированию даты!\n\n'
                         f'Ниже я напишу тебе, как это сделать!', reply_markup=ReplyKeyboardRemove())
    await asyncio.sleep(2)
    await message.answer(f"Напиши дату которую мне надо запомнить.\n\n"
                         f"Ты можешь мне написать дату в формате <i>День.Месяц</i> или <i>День,Месяц</i>,"
                         f" чтоб я напомнил тебе только о приближении этого дня.\n\n"
                         f"Или ты можешь написать дату в формате <i>День.Месяц.Год</i> или <i>День,Месяц,Год</i>, "
                         f"чтоб я напомнил тебе о приближении этого дня"
                         f" и посчитал сколько исполнится лет этом году.\n\n"
                         f"<b>Пиши дату через точку или запятую, так чтоб я смог тебя понять</b>",
                         reply_markup=cancel_btn)


@dp.message_handler(state=NewHbd.date_hbd)
async def date_hbd(message: types.Message, state: FSMContext):
    logging.info(f'{message.from_user.full_name} in {await state.get_state()} write {message.text}')
    text = message.text
    text = re.split('[,.]', text)
    if len(text) == 2 or len(text) == 3:
        try:
            if len(text) == 2:
                if 1 < int(text[0]) > 31:
                    logging.warning(f'{message.from_user.full_name} have a mistake in DAY')
                    raise ValueError
                if 1 < int(text[1]) > 12:
                    logging.warning(f'{message.from_user.full_name} have a mistake in MONTH')
                    raise ValueError
            elif len(text) == 3:
                if 1 < int(text[0]) > 31:
                    logging.warning(f'{message.from_user.full_name} have a mistake in DAY')
                    raise ValueError
                if 1 < int(text[1]) > 12:
                    logging.warning(f'{message.from_user.full_name} have a mistake in MONTH')
                    raise ValueError
                if int(text[2]) > 9999:
                    logging.warning(f'{message.from_user.full_name} have a mistake in YEAR')
                    raise ValueError

            async with state.proxy() as data:
                data['date_hbd'] = message.text
            await NewHbd.str_hbd.set()
            await message.reply("А теперь напиши мне, как называется этот праздник.",
                                reply_markup=cancel_btn)
        except ValueError:
            logging.warning(f'{message.from_user.full_name} have a mistake with data')
            await message.reply("Ты ввел некорректные данные, возможно ты где то ввел буквы"
                                " прочитай сообщение о том как вводить данные еще раз. 😉\n\n"
                                "Попробуй еще раз ввести дату или нажми на <b>Отмена</b>",
                                reply_markup=cancel_btn)
            await NewHbd.date_hbd.set()

    elif len(text) == 1:
        logging.warning(f'{message.from_user.full_name} have a mistake len() == 1')
        await message.reply("Ты ввел не правильное значение. Посмотри внимательно,"
                            " <i>возможно ввел только одно число</i>. 😉\n\n"
                            "Попробуй еще раз ввести дату или нажми на <b>Отмена</b>",
                            reply_markup=cancel_btn)
        await NewHbd.date_hbd.set()
    else:
        logging.warning(f'{message.from_user.full_name} have an another mistake')
        await message.reply("Ты ввел не правильное значение. Посмотри внимательно, какое сообщение я хочу получить "
                            "от тебя и отправь мне.\n\n"
                            "Попробуй еще раз ввести дату или нажми на <b>Отмена</b>",
                            reply_markup=cancel_btn)
        await NewHbd.date_hbd.set()


@dp.message_handler(state=NewHbd.str_hbd)
async def str_hbd(message: types.Message, state: FSMContext):
    logging.info(f'{message.from_user.full_name} in {await state.get_state()} write {message.text}')
    async with state.proxy() as data:
        data['str_hbd'] = message.text

    await message.answer(f'Проверь, правильно ли я записал:\n\n'
                         f'{data["date_hbd"]} - {data["str_hbd"]}',
                         reply_markup=keyboard)

    await NewHbd.bool_state.set()


@dp.callback_query_handler(text_contains="yes", state=NewHbd.bool_state)
async def confirm_hbd(call: types.CallbackQuery, state: FSMContext):
    logging.info(f'{call.from_user.full_name} in {await state.get_state()} say YES')
    await call.message.edit_reply_markup()
    data = await state.get_data()
    date = data['date_hbd']
    date = re.split('[,.]', date)
    if len(date) == 3:
        day = int(date[0])
        month = int(date[1])
        year = int(date[2])
    else:
        day = int(date[0])
        month = int(date[1])
        year = 0
    user_id = call.from_user.id
    db.add_new_hbd(user_id, name=data['str_hbd'], day=day, month=month, year=year)
    await call.message.answer('<b>Запись успешно внесена</b>', reply_markup=main_menu)
    await state.finish()


@dp.callback_query_handler(text_contains="no", state=NewHbd.bool_state)
async def non_confirm_hbd(call: types.CallbackQuery, state: FSMContext):
    logging.info(f'{call.from_user.full_name} in {await state.get_state()} say NO')
    await call.message.edit_reply_markup()
    await call.message.answer("Начнем с самого начала.\n\n"
                              "Напиши мне дату праздника.", reply_markup=cancel_btn)
    await NewHbd.date_hbd.set()
