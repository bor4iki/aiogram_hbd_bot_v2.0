from aiogram import types
from aiogram.dispatcher.filters import Text
from aiogram.dispatcher.filters.builtin import CommandHelp

from loader import dp, db
from utils.misc.add_new_user import add_user_to_db
from utils.misc.logs import logging


@dp.message_handler(CommandHelp())
@dp.message_handler(Text(equals='Помощь'))
async def bot_help(message: types.Message):
    logging.info(f'{message.from_user.full_name} start command /help')
    await add_user_to_db(db, message)
    text = ("Список команд: ",
            "/start - Начать диалог",
            "/help - Получить справку",
            "/new - Записать новую дату",
            "/view_all_notes - Посмотреть все записи",
            "/nearest_notes - Посмотреть записи в этом месяце",
            )
    
    await message.answer("\n".join(text))
