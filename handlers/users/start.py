from aiogram import types
from aiogram.dispatcher.filters import Text
from aiogram.dispatcher.filters.builtin import CommandStart

from keyboards.default.main_keyboard import main_menu

from loader import dp, db
from utils.misc.logs import logging
from utils.misc.add_new_user import add_user_to_db


@dp.message_handler(CommandStart())
@dp.message_handler(Text(equals='Начальное сообщение'))
async def bot_start(message: types.Message):
    start_text = 'Приветствую тебя в телеграмм боте!🤗\n\n' \
        'С моей помощью ты не забудешь о важных датах.\n\n' \
        'Я буду напоминать о скорой необходимости  подготовки  поздравления' \
        ' и подарка для твоих близких и друзей 🎁\n\n' \
        'Все, что мне от тебя требуется - это следовать простым правилам.\n\n' \
        'Надеюсь я тебя не подведу.\n' \
        'Удачи в пользовании меня! 💪🏻'
    logging.info(f'{message.from_user.full_name} start command /start')
    await add_user_to_db(db, message)
    await message.answer(start_text, reply_markup=main_menu)
