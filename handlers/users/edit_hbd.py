import re

from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import RegexpCommandsFilter, Text
from aiogram.types import ReplyKeyboardRemove

from keyboards.default.main_keyboard import main_menu
from keyboards.inline.cancel_btn import cancel_btn
from keyboards.inline.yesorno import keyboard
from loader import dp, db
from utils.misc.add_new_user import add_user_to_db
from utils.misc.logs import logging
from keyboards.inline.edit_hbd_text_btn import edit_keyboard
from states.edit_hbd_state import EditName, EditDate


@dp.message_handler(RegexpCommandsFilter(regexp_commands=['edit_([0-9]*)']), state=None)
async def edit_message(message: types.Message, state: FSMContext):
    id_ = 0
    user_id = message.from_user.id
    edit_num = message.text.split('_')[1]
    logging.info(f'{message.from_user.full_name} - call /edit_{edit_num}')
    await add_user_to_db(db, message)
    notes = db.get_hbd(user_id)
    count = 1
    text = 'Редактирование записи\n\n'
    for note in notes:
        if count == int(edit_num):
            id_, name, day, month, year = note
            if year != 0:
                text += f'<b>Номер записи :</b> <i>{count}</i>\n' \
                        f'<b>Название :</b> <i>{name}</i>\n' \
                        f'<b>Дата :</b> <i>{day}.{month}.{year}</i>\n'
            else:
                text += f'<b>Номер записи :</b> <i>{count}</i>\n' \
                        f'<b>Название :</b> <i>{name}</i>\n' \
                        f'<b>Дата :</b> <i>{day}.{month}</i>\n'
        count += 1
    async with state.proxy() as data:
        data['id_'] = id_
    text += '\nНиже выбери то, что ты хочешь изменить.\n' \
            'Так же ты можешь удалить эту запись!'
    await message.answer(text, reply_markup=edit_keyboard)


# @dp.callback_query_handler(text_contains="name", state=None)
@dp.callback_query_handler(Text(equals=['name', 'date']), state=None)
async def edit_name_date_set(call: types.CallbackQuery, state: FSMContext):
    call_data = call.data
    data = await state.get_data()
    id_ = data['id_']
    logging.info(f'{call.from_user.full_name} want change name of HBD id -> {id_}')
    await call.message.edit_reply_markup()
    if str(call_data) == 'name':
        await EditName.name.set()
        await call.message.answer("Напиши мне новое название.\n\n", reply_markup=ReplyKeyboardRemove())
    elif str(call_data) == 'date':
        await EditDate.date.set()
        await call.message.answer("Напиши мне новую дату.\n\n", reply_markup=ReplyKeyboardRemove())


@dp.message_handler(state=[EditName.name, EditDate.date])
async def edited_name_date_db(message: types.Message, state: FSMContext):
    set_state = await state.get_state()
    logging.info(f'└---->{message.from_user.full_name} in {set_state} change name of HBD')
    mes = message.text
    text = f'<i>Проверь правильно ли ты все написал:</i> \n\n' \
           f'{mes}'
    if str(set_state) == 'EditName:name':
        async with state.proxy() as data:
            data['name'] = message.text
        await EditName.bool_state.set()
        await message.answer(text, reply_markup=keyboard)
    elif str(set_state) == 'EditDate:date':
        mess = re.split('[,.]', mes)
        if len(mess) == 2 or len(mess) == 3:
            try:
                if len(mess) == 2:
                    if 1 < int(mess[0]) > 31:
                        logging.warning(f'{message.from_user.full_name} have a mistake in DAY')
                        raise ValueError
                    if 1 < int(mess[1]) > 12:
                        logging.warning(f'{message.from_user.full_name} have a mistake in MONTH')
                        raise ValueError
                elif len(mess) == 3:
                    if 1 < int(mess[0]) > 31:
                        logging.warning(f'{message.from_user.full_name} have a mistake in DAY')
                        raise ValueError
                    if 1 < int(mess[1]) > 12:
                        logging.warning(f'{message.from_user.full_name} have a mistake in MONTH')
                        raise ValueError
                    if int(mess[2]) > 9999:
                        logging.warning(f'{message.from_user.full_name} have a mistake in YEAR')
                        raise ValueError

                async with state.proxy() as data:
                    data['date'] = message.text
                await EditDate.bool_state.set()
                await message.answer(text, reply_markup=keyboard)
            except ValueError:
                logging.warning(f'{message.from_user.full_name} have a mistake with data')
                await message.reply("Ты ввел некорректные данные, возможно ты где то ввел буквы"
                                    " прочитай сообщение о том как вводить данные еще раз. 😉\n\n"
                                    "Попробуй еще раз ввести дату или нажми на <b>Отмена</b>",
                                    reply_markup=cancel_btn)
                await EditDate.date.set()

        elif len(mess) == 1:
            logging.warning(f'{message.from_user.full_name} have a mistake len() == 1')
            await message.reply("Ты ввел не правильное значение. Посмотри внимательно,"
                                " <i>возможно ввел только одно число</i>. 😉\n\n"
                                "Попробуй еще раз ввести дату или нажми на <b>Отмена</b>",
                                reply_markup=cancel_btn)
            await EditDate.date.set()
        else:
            logging.warning(f'{message.from_user.full_name} have an another mistake')
            await message.reply("Ты ввел не правильное значение. Посмотри внимательно, какое сообщение я хочу получить "
                                "от тебя и отправь мне.\n\n"
                                "Попробуй еще раз ввести дату или нажми на <b>Отмена</b>",
                                reply_markup=cancel_btn)
            await EditDate.date.set()


@dp.callback_query_handler(text_contains="yes", state=[EditName.bool_state, EditDate.bool_state])
async def confirm_hbd(call: types.CallbackQuery, state: FSMContext):
    set_state = await state.get_state()
    logging.info(f'    └---->{call.from_user.full_name} in {set_state} say YES')
    await call.message.edit_reply_markup()
    data = await state.get_data()
    id_ = data['id_']
    await call.message.answer('<b>Запись успешно внесена</b>', reply_markup=main_menu)
    if str(set_state) == 'EditName:bool_state':
        name = data['name']
        db.update_name(id_, name)
    elif str(set_state) == 'EditDate:bool_state':
        date = str(data['date'])
        date = re.split('[,.]', date)
        if len(date) == 3:
            day = int(date[0])
            month = int(date[1])
            year = int(date[2])
        else:
            day = int(date[0])
            month = int(date[1])
            year = 0
        db.update_date(id_, day, month, year)
    text = ''
    notes = db.get_hbd_from_id(id_)
    for note in notes:
        name, day, month, year = note
        if year != 0:
            text += f'<b>Название :</b> <i>{name}</i>\n' \
                    f'<b>Дата :</b> <i>{day}.{month}.{year}</i>\n'
        else:
            text += f'<b>Название :</b> <i>{name}</i>\n' \
                    f'<b>Дата :</b> <i>{day}.{month}</i>\n'
    await call.message.answer(f'<i>Запись с новым названием теперь выглядит так:</i>\n\n'
                              f'{text}')
    await state.finish()


@dp.callback_query_handler(text_contains="no", state=[EditName.bool_state, EditDate.bool_state])
async def non_confirm_hbd(call: types.CallbackQuery, state: FSMContext):
    set_state = await state.get_state()
    await call.message.edit_reply_markup()
    logging.info(f'    └---->{call.from_user.full_name} in {set_state} say NO')
    if str(set_state) == 'EditName:bool_state':
        await call.message.answer("Давай попробуем еще раз.\n\n"
                                  "Напиши мне новое название.", reply_markup=cancel_btn)
        await EditName.name.set()
    elif str(set_state) == 'EditDate:bool_state':
        await call.message.answer("Давай попробуем еще раз.\n\n"
                                  "Напиши мне новую дату.", reply_markup=cancel_btn)
        await EditDate.date.set()
