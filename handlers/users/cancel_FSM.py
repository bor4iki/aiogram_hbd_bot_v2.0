from aiogram.dispatcher import FSMContext
from aiogram.types import CallbackQuery

from loader import dp
from states.add_new_hbd import NewHbd
from states.edit_hbd_state import EditDate, EditName
from utils.misc.logs import logging
from keyboards.default.main_keyboard import main_menu


@dp.callback_query_handler(text_contains="cancel", state=[NewHbd, EditDate, EditName, None])
async def cancel_handler(call: CallbackQuery, state: FSMContext):
    """
    Allow user to cancel any action
    """
    await call.message.edit_reply_markup()
    current_state = await state.get_state()
    if current_state is None:
        logging.info(f'{call.from_user.full_name} cancelling state {current_state}')
        await call.message.answer('<b>Отмена выполнения предыдущего действия.</b>')
        return

    logging.info(f'{call.from_user.full_name} cancelling state {current_state}')
    # Cancel state and inform user about it
    await state.finish()
    if current_state == 'NewHbd:date_hbd':
        current_state = 'Заполнение даты'
    elif current_state == 'NewHbd:str_hbd':
        current_state = 'Заполнение названия'
    elif current_state == 'NewHbd:bool_state':
        current_state = 'Проверка правильности'
    elif current_state == 'EditDate:bool_state' or current_state == 'EditDate:date':
        current_state = 'Изменение даты'
    elif current_state == 'EditName:bool_state' or current_state == 'EditName:name':
        current_state = 'Изменение названия'
    # And remove keyboard (just in case)
    await call.message.answer(f'Вы отменили запись в мою базу данных на уровне : <b>{current_state}</b>\n\n'
                              f'Ничего страшного в этом нет, вы так же можете заново начать запись, введя телеграмм '
                              f'команду /new', reply_markup=main_menu)
