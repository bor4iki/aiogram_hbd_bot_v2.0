from aiogram import types
from aiogram.dispatcher import FSMContext

from keyboards.default.main_keyboard import main_menu
from loader import dp, db
from utils.misc.logs import logging


@dp.callback_query_handler(text_contains="delete", state=None)
async def delete_line_hbd(call: types.CallbackQuery, state: FSMContext):
    data = await state.get_data()
    await call.message.edit_reply_markup()
    id_ = data['id_']
    logging.info(f'{call.from_user.full_name} delete HBD id -> {id_}')
    db.delete_line(id_)
    await call.message.answer("<b>Удаление прошло успешно!</b>", reply_markup=main_menu)
