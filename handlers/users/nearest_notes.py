from datetime import datetime

from aiogram import types
from aiogram.dispatcher.filters import Command, Text

from loader import dp, db
from utils.misc.add_new_user import add_user_to_db
from utils.misc.logs import logging


def get_month():
    return datetime.now().month


def get_day():
    return datetime.now().day


@dp.message_handler(Command('nearest_notes'))
@dp.message_handler(Text(equals='Записи в этом месяце'))
async def view_all_notes(message: types.Message):
    user_id = message.from_user.id
    logging.info(f'{message.from_user.full_name} - call command /nearest_notes')
    await add_user_to_db(db, message)
    notes = db.get_hbd(user_id)
    text = '<b>В этом месяце тебя ждут такие даты!</b>\n\n' \
           '<i>Название    -    Дата</i>\n\n'
    count = 1
    for note in notes:
        id_, name, day, month, year = note
        if month == get_month():
            if year != 0:
                text += f'{count}.  {name}   -   {day}.{month}.{year}    {"✅" if day < get_day() else "🔜"}\n' \
                        f'----------------------------------------------\n'
                count += 1
            else:
                text += f'{count}.  {name}   -   {day}.{month}    {"✅" if day < get_day() else "🔜"}\n' \
                        f'----------------------------------------------\n'
                count += 1
    await message.answer(text)
