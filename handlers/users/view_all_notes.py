from aiogram import types
from aiogram.dispatcher.filters import Command, Text

from loader import dp, db
from utils.misc.add_new_user import add_user_to_db
from utils.misc.logs import logging


@dp.message_handler(Command('view_all_notes'))
@dp.message_handler(Text(equals='Все записи'))
async def view_all_notes(message: types.Message):
    user_id = message.from_user.id
    logging.info(f'{message.from_user.full_name} - call /view_all_notes')
    await add_user_to_db(db, message)
    notes = db.get_hbd(user_id)
    text = f'<b>Твои записи:</b>\n\n' \
           f'<i>Название    -    Дата</i>\n\n'
    count = 1
    for note in notes:
        id_, name, day, month, year = note
        if len(text) >= 3600:
            await message.answer(text)
            text = f'<b>Твои записи:</b>\n\n' \
                   f'<i>Название    -    Дата</i>\n\n'
        if year != 0:
            text += f'{count}.  {name}   -   {day}.{month}.{year}\n' \
                    f'---ред. запись {count} - /edit_{count}\n' \
                    f'----------------------------------------------\n'
            count += 1
        else:
            text += f'{count}.  {name}   -   {day}.{month}\n' \
                    f'---ред. запись {count} - /edit_{count}\n' \
                    f'----------------------------------------------\n'
            count += 1
    await message.answer(text)
