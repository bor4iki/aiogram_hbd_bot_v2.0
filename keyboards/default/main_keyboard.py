from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

main_menu = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text='Начальное сообщение'),
            KeyboardButton(text='Помощь'),
        ],
        [
            KeyboardButton(text='Добавить новую запись'),
        ],
        [
            KeyboardButton(text='Все записи'),
            KeyboardButton(text='Записи в этом месяце'),
        ],
    ],
    resize_keyboard=True
)
