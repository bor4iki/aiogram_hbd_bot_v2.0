from aiogram.types.inline_keyboard import InlineKeyboardButton, InlineKeyboardMarkup

edit_keyboard = InlineKeyboardMarkup(
    inline_keyboard=
    [
        [
            InlineKeyboardButton(text="Название", callback_data="name"),
            InlineKeyboardButton(text="Дата", callback_data="date"),
            InlineKeyboardButton(text="Удалить", callback_data="delete")
        ],
        [
            InlineKeyboardButton(text="Отмена", callback_data="cancel")
        ],
    ]
)

