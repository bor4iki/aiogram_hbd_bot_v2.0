from aiogram.types.inline_keyboard import InlineKeyboardButton, InlineKeyboardMarkup


keyboard = InlineKeyboardMarkup(
        inline_keyboard=
        [
            [InlineKeyboardButton(text="Да", callback_data="yes"),
             InlineKeyboardButton(text="Неть", callback_data="no")],
            [InlineKeyboardButton(text="Отмена", callback_data="cancel")],
        ]
)
