import shutil


class RezervCopy:

    def __init__(self, filename):
        self.filename = filename

    async def run(self):
        copyname = f'{self.filename}_copy.db'
        await shutil.copy2(self.filename, copyname)


# copy = RezervCopy('bot.db')
# copy.run()
