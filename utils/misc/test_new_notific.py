from datetime import datetime, timedelta

from utils.db_api.db_main import SQLighter


# from loader import db
# from rezerv_copy import RezervCopy

# copy = RezervCopy('bot.db')


class Notification:

    def __init__(self, database: SQLighter, bot):
        self.db = database
        self.bot = bot

    @staticmethod
    def date_now():
        return datetime.now()

    async def run(self):
        # print(f'{datetime.now()} + {self.date_now()}')
        await self.take_date()
        text_iteration = f'Операция - была выполнена {self.date_now()}'
        print(text_iteration)

    def plus_delta(self, days: int):
        return self.date_now() + timedelta(days)

    @staticmethod
    def get_date(year: int, month: int, day: int):
        return datetime(year, month, day)

    async def take_date(self):
        values = self.db.get_all_hbd()
        date_now_ = self.date_now().strftime('%d-%m')
        for value in values:
            user_id, text_date, day, month, year = value
            if year != 0:
                date_time_obj = self.get_date(year, month, day).strftime('%d-%m')
            else:
                date_time_obj = self.get_date(1, month, day).strftime('%d-%m')
            if date_now_ == date_time_obj:
                message = f'🎉🎉🎉 Сегодня 🎉🎉🎉' \
                          f'УРА!\n\n' \
                          f'Сегодня: {text_date}.\n'
                message_year = f'С рождения прошло: {str(self.date_now().year - year)} лет'
                await self.bot.send_message(user_id,
                                            f'{message} {message_year if year != 0 else ""}')
                print(f'- - - - {user_id} - {text_date} - - - -')
            if self.plus_delta(1).strftime('%d-%m') == date_time_obj:
                message = f'Остался 1️⃣ день!\n' \
                          f'Нужно готовиться, а то будет поздно!!\n\n' \
                          f'Завтра: {text_date}.\n'
                message_year = f'Стукнет: {str(self.date_now().year - year)} лет'
                # print(user_id, f'{message} {message_year if year != 0 else ""}')
                await self.bot.send_message(user_id,
                                            f'{message} {message_year if year != 0 else ""}')

            if self.plus_delta(3).strftime('%d-%m') == date_time_obj:
                message = f'Осталось 3️⃣ дня!\n' \
                          f'Я хочу предупредить тебя!!!\n\n' \
                          f'У тебя осталось ровно три дня до: {text_date}.\n'
                message_year = f'Стукнет: {str(self.date_now().year - year)} лет'
                # print(user_id, f'{message} {message_year if year != 0 else ""}')
                await self.bot.send_message(user_id,
                                            f'{message} {message_year if year != 0 else ""}')
            if self.plus_delta(7).strftime('%d-%m') == date_time_obj:
                message = f'Осталось 7️⃣ дней!\n' \
                          f'Время еще есть, но нужно начинать думать над подарком\n\n' \
                          f'Через 7 дней произойдёт: {text_date}.\n'
                message_year = f'Стукнет: {str(self.date_now().year - year)} лет'
                # print(user_id, f'{message} {message_year if year != 0 else ""}')

                await self.bot.send_message(user_id,
                                            f'{message} {message_year if year != 0 else ""}')


# notifi = Notification()
# schedule.every().day.at("14:39").do(notifi.run)
# schedule.every().day.at("10:00").do(copy.run)
if __name__ == '__main__':
    print(f'-------Начало программы {datetime.now()}-------')
    from loader import db
    # noti = Notification(db)
    # noti.run()
    # while True:
    #     try:
    #         schedule.run_pending()
    #         time.sleep(1)
    #     except KeyboardInterrupt as key:
    #         print('-------Программа завершилась с помощью клавиатуры!-------')
    #         break
