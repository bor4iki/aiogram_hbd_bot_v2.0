from aiogram import types


async def set_default_commands(dp):
    await dp.bot.set_my_commands(
        [
            types.BotCommand("start", "Запустить бота"),
            types.BotCommand("help", "Вывести справку"),
            types.BotCommand("new", "Добавить новую дату"),
            types.BotCommand("view_all_notes", "Посмотреть все записи"),
            types.BotCommand("nearest_notes", "Посмотреть даты в этом месяце"),
        ]
    )
