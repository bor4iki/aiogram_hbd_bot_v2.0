from aiogram.dispatcher.filters.state import State, StatesGroup


class EditName(StatesGroup):
    id_ = State()
    name = State()
    bool_state = State()


class EditDate(StatesGroup):
    id_ = State()
    date = State()
    bool_state = State()
